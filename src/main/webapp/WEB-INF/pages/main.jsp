<%@ include file="taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=utf-8" %>
<meta charset='utf-8'>
<!DOCTYPE html>

<html lang="en" manifest="/cache.manifest">
<head>

</head>
<body>

<div class="container">

    <br>
    <br>

    <div class="row main_content">

        <div class="span3">
            <div class="well">
                    <nav>
                        <ul id="categoryList" class="nav nav-list">
                            <li class="nav-header" disable="disable">
                                input information for graph
                            </li>
                            <li>
                                here is some data
                                <input type="text"/>
                            </li>
                            <li>
                                <input type="submit" class="btn btn-primary btn-block" value="Рисовать">
                            </li>
                            <li>
                                <button class="btn btn-primary btn-block" onclick="switchWebOnOff()">Сетка</button>
                            </li>
                            <li>
                                <button class="btn btn-primary btn-block" onclick="getGraph('sin')">sin</button>
                            </li>
                            <li>
                                <button class="btn btn-primary btn-block" onclick="getGraph('cos')">cos</button>
                            </li>
                        </ul>
                    </nav>
            </div>
        </div>

        <div class="span9">
            <div id="fileInfoContent" class="well text-center">
                <jsp:include page="graph.jsp" />
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript">

    var prePath = <c:url value="/"/>;
    function getGraph(value){

        /*here get some data from input forms*/

        var id = $(this).attr("id");
        $.ajax({type: "POST",
            url: prePath + '/graph',
            data: "somedata=" + value,
            success: function (responseHtml) {
                gcm(0);
                graphArray[0] = responseHtml;
                rewriteMaxValues();
                redrawCanvas();
            },
            error: function (){

            }
        });
    }






</script>
</html>