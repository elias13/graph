<%@ include file="taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=utf-8" %>
<meta charset='utf-8'>
<!DOCTYPE html>

<html lang="en" manifest="/cache.manifest">
<head>

</head>
<body>

<div class="container">

    <br>
    <br>

    <div class="row main_content">

        <div class="span3">
            <div class="well">
                    <nav>
                        <ul id="categoryList" class="nav nav-list">
                            <li class="nav-header" disable="disable">
                                input data here
                            </li>
                            <li>
                                time
                                <input id="time" type="text" value="1.3"/>
                            </li>
                            <li>
                                function=F(x,y)
                                <input id="function" type="text" value="Sin[Pi*x]*Sin[Pi*y]"/>
                            </li>
                            <li>
                                dCoef
                                <input id="dCoef" type="text" value="1"/>
                            </li>
                            <li>
                                vCoef
                                <input id="vCoef" type="text" value="0"/>
                            </li>
                            <li>
                                <button class="btn btn-primary btn-block" onclick="getGraph()">build</button>
                            </li>
                        </ul>
                    </nav>
            </div>
        </div>

        <div class="span9">
            <div id="container" class = "well">

            </div>
        </div>

        <div class="span9">
            <div id="container2" class = "well">
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript">

    // standard global variables
    var container, scene, camera, renderer, controls
    var clock = new THREE.Clock();
    // custom global variables
    var cube;
    var sphereGeom = new THREE.SphereGeometry( 3, 32, 16 );
    var redMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, transparent: true, opacity: 0.5 } );
    //var graphParams = [new THREE.Vector3(1,2,3), new THREE.Vector3(5,3,6), new THREE.Vector3(3,2,3), new THREE.Vector3(0,3,9)];
      var graphParams = ${graphParams};

    init($("#container"));
    animate();

    function generateVectors(vertexes){
        var vectorsArray = new Array();
        for (var i = 0 ; i < vertexes.length; i++){
            vectorsArray[i] = new THREE.Vector3(vertexes[i][0], vertexes[i][1], vertexes[i][2]);
        }
        return vectorsArray;
    }

    function getZ(x, y){
        //x = xRange * x + xMin;
        //y = yRange * y + yMin;
        var controlSum = 0.04;
        var z = 0;
            for(var i = 0; i < graphParams.length; i++){
                if(Math.abs(graphParams[i][0] - x + graphParams[i][1] - y) < controlSum){
                    z = graphParams[i][2];
                }
                if(graphParams[i][0] - x > 0.02
                        && graphParams[i][1] - y > 0.02){
                    break;
                }
            }

        x = x*10.0;
        y = y*10.0;
        z = z*10.0;

        if ( isNaN(z) ){
            z = z || 1;
            return new THREE.Vector3(x,y,0); // TODO: better fix
        }
        else
            return new THREE.Vector3(x, y, z);
    }

    var prePath = <c:url value="/"/>;
    function getGraph(){

        /*here get some data from input forms*/
        var time = $("#time").val();
        var func = $("#function").val();
        var dCoef = $("#dCoef").val();
        var vCoef = $("#vCoef").val();
        var id = $(this).attr("id");
        $.ajax({type: "POST",
            url: prePath + 'graph/'+ func + '/' + time + '/' + dCoef + '/' + vCoef,
            success: function (responseHtml) {
                graphParams = jQuery.parseJSON(responseHtml)    ;
                //rewriteMaxValues();
                //redrawCanvas();
                createGraph();
            },
            error: function (){

            }
        });
    }





</script>
</html>