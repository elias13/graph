<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script src='<c:url value = "/js/jquery.js"/>'></script>
<script src='<c:url value = "/js/jcanvas.js"/>'></script>
<script src='<c:url value = "/js/three.js"/>'></script>
<script src='<c:url value = "/js/Detector.js"/>'></script>
<script src='<c:url value = "/js/Stats.js"/>'></script>
<script src='<c:url value = "/js/TrackballControls.js"/>'></script>
<script src='<c:url value = "/js/THREEx.KeyboardState.js"/>'></script>
<script src='<c:url value = "/js/THREEx.FullScreen.js"/>'></script>
<script src='<c:url value = "/js/THREEx.WindowResize.js"/>'></script>
<script src='<c:url value = "/js/DAT.GUI.min.js"/>'></script>
<script src='<c:url value = "/js/parser.js"/>'></script>
<script src='<c:url value = "/js/graph.js"/>'></script>
<%--<script src='<c:url value = "/js/tquery-all.js"/>'></script>--%>
<script type = "text/javascript" src='<c:url value = "/js/main.js"/>'></script>
<link href='<c:url value = "/css/bootstrap.css"/>' rel="stylesheet" type="text/css"/>
<link href='<c:url value = "/css/main.css"/>' rel="stylesheet" type="text/css"/>


