<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<canvas id="graphBody" width="600" height="600" class="graph">
    Your browser does not support the HTML5 canvas tag.
</canvas>

<script type="text/javascript">

    $( function(){
        drawSomething();
        redrawCanvas();
    });




    function drawSomething(){
        var obj = {
            strokeStyle: "#000",
            strokeWidth: 6,
            rounded: true
        };

        var pts = [
            [80, 50],
            [100, 150],
            [200, 100],
            [150, 200]
        ];

        for (var p = 0; p < pts.length; p += 1) {
            obj['x' + (p + 1)] = pts[p][0];
            obj['y' + (p + 1)] = pts[p][1];
        }
        $("canvas").drawLine(obj);

        /*$("canvas").drawText({
            fillStyle: "#9cf",
            strokeStyle: "#25a",
            strokeWidth: 2,
            x: 150, y: 100,
            fontSize: "36pt",
            fontFamily: "Verdana, sans-serif",
            text: 1323
        });*/
    }


    /*var arr = jQuery.parseJSON(json_text);*/


</script>