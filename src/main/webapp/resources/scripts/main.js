var GRID_LAYER = "grid layer";
var COORDINATE_LAYER = "coordinates main layer";
var GRID_ON = false;
var graphArray = new Array();
var colorHeap = ["black", "blue", "yellow", "green", "red", "lime", "purple", "darkblue", "greenyellow", "violet"]
var maxX = 100;
var maxY = 100;
var minX = -100;
var minY = -100;


function redrawCanvas(){
    $("canvas").clearCanvas();
    gridInit();
    coordinatesInit();
    drawGraphs();
}

function switchWebOnOff(){
    GRID_ON = !GRID_ON;
    redrawCanvas();
}

function gridInit(){
    if(GRID_ON){
        for(var i = 0; i < 21; i += 1){
            $("canvas").drawLine({
                layer:true,
                group:GRID_LAYER,
                strokeStyle: "#000",
                strokeWidth: 0.1,
                x1:i*30, y1:0,
                x2:i*30, y2:600
            });
            $("canvas").drawLine({
                layer:true,
                group:GRID_LAYER,
                strokeStyle: "#000",
                strokeWidth: 0.1,
                x1:0, y1:(i * 30),
                x2:600, y2:(i * 30)
            });
        }
    }
}

function coordinatesInit(){

    for(var i = 0; i < 21; i += 1){
        $("canvas").drawLine({
            layer:true,
            group:COORDINATE_LAYER,
            strokeStyle: "#000",
            strokeWidth: 1,
            x1:i*30, y1:298,
            x2:i*30, y2:302
        });
        $("canvas").drawLine({
            layer:true,
            group:COORDINATE_LAYER,
            strokeStyle: "#000",
            strokeWidth: 1,
            layer:true,
            x1:299, y1:(i * 30),
            x2:301, y2:(i * 30)
        });

        $("canvas").drawText({
            layer:true,
            group:COORDINATE_LAYER,
            fillStyle: "#9cf",
            strokeWidth: 2,
            x: i*30, y: i%2==0?308:292,
            fontSize: "10pt",
            fontFamily: "Verdana, sans-serif",
            text: cT(minX, maxX, i)
        });

        if(i != 10){
            $("canvas").drawText({
                layer:true,
                group:COORDINATE_LAYER,
                fillStyle: "#9cf",
                strokeWidth: 2,
                x:290, y:(i * 30),
                fontSize: "10pt",
                fontFamily: "Verdana, sans-serif",
                text: cT(maxY, minY, i)
            });
        }
    }

    $("canvas").drawLine({
        layer:true,
        group:COORDINATE_LAYER,
        strokeStyle: "#f00",
        strokeWidth: 0.1,
        x1: 300, y1:0,
        x2: 300, y2:600
    });
    $("canvas").drawLine({
        layer:true,
        group:COORDINATE_LAYER,
        strokeStyle: "#f00",
        strokeWidth: 0.1,
        x1: 0, y1:300,
        x2: 600, y2:300
    });
}

//coordinate text
function cT(min, max, i){
    var all = max - min;
    return min + (all - all%20)/20*i
}

function drawGraphs(){
    for( var i = 0; i < graphArray.length; i++){
        drawSingleGraph(graphArray[i], colorHeap[rnd()]);
    }
}
function rnd(){
    return ( 1 + Math.floor(Math.random()*10));
}

//with autorotate Y
function drawSingleGraph(points, color){
    if(color == null){
        color = "black";
    }
    var obj = {
        strokeStyle: color,
        strokeWidth: 2,
        rounded: true
    };

    var pts = jQuery.parseJSON(points);

    for (var p = 0; p < pts.length; p += 1) {
        obj['x' + (p + 1)] = scaleByX(pts[p][0]);
        obj['y' + (p + 1)] = scaleByY(pts[p][1]);
    }
    $("canvas").drawLine(obj);
}

function scaleByX(coord){
    return (coord - minX)*600/(maxX - minX)
}

function scaleByY(coord){
    return (maxY - coord)*600/(maxY - minY)
}

function rewriteMaxValues(){
    var pts = jQuery.parseJSON(graphArray);

    for (var p = 0; p < pts.length; p += 1) {
        if(pts[p][0] > maxX){
            gcm(pts[p][0]);
        } else if(pts[p][0] < minX){
            gcm(-1 * pts[p][0]);
        }
        if(pts[p][1] > maxY){
            gcm(pts[p][1]);
        } else if(pts[p][1] < minY){
            gcm(-1 * pts[p][1]);
        }
    }
}

//global change max value
function gcm(max){
    maxX = max;
    minX = -max;
    maxY = max;
    minY = -max;
}








