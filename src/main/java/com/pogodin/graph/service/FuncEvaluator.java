package com.pogodin.graph.service;

import com.pogodin.graph.APIException;
import org.matheclipse.parser.client.eval.DoubleEvaluator;
import org.matheclipse.parser.client.eval.DoubleVariable;
import org.matheclipse.parser.client.eval.IDoubleValue;

public class FuncEvaluator {
    private String func;
    DoubleEvaluator engine;
    private IDoubleValue xInFunc;
    private IDoubleValue yInFunc;


    public FuncEvaluator(String func) {
        this.func = func;
        engine = new DoubleEvaluator();
        xInFunc = new DoubleVariable(0.0);
        yInFunc = new DoubleVariable(0.0);
        init();
        //todo exception here
        engine.evaluate(func);
    }

    public void init(){
        engine.defineVariable("x", xInFunc);
        engine.defineVariable("y", yInFunc);
    }

    public double evaluate(Double x, Double y){

        if(x == null || y == null){
            throw new APIException("x and y should not be null: x = " + x + "; y = " + y);
        }
        try{
            xInFunc.setValue(x);
            yInFunc.setValue(y);
            return engine.evaluate();
        } catch (ArithmeticException ex){
            throw new APIException("something wrong in parse function on x = " + x + "y = " + y
            + "function is : " + func + "  \n\n "+ ex.getStackTrace());
        }
    }
}
