package com.pogodin.graph.service.impl;

import com.pogodin.graph.model.Graph;
import com.pogodin.graph.model.Point;
import com.pogodin.graph.service.FuncEvaluator;
import com.pogodin.graph.service.GraphService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class GraphServiceImpl implements GraphService {


    public Graph generateTestGraph() {

        Graph result = new Graph();
        Random rand = new Random();
        int lastYpoint = rand.nextInt(600);
        for (int i = -100 ; i < 100; i++){
            result.addNext(i, i*i, i*i*i);
        }

        return result;
    }

    public Graph generateGraph(String s) {
        if(s.matches("cos")){
            return cos3();
        }

        return new Graph();
    }

    // sin(sqrt(a*x^2  + b*y^2))
    private Graph cos3(){
        Graph result = new Graph();
        for(double x = 0; x < 10; x += 0.1){
            for (double y = 0; y < 10; y += 0.1){
                result.addNext(x , y, Math.sin(Math.sqrt(x*x + y*y)));
            }
        }
        return result;
    }


    @Override
    public Graph resolveCoordMethod(final double [] uI, final double dCoef,
                                     final double vCoef,
                                     final double resultTime,
                                     final String function){

        final FuncEvaluator func = new FuncEvaluator(function);
        final double deltaH = 1/SEP_PART_SIZE;
        if(uI.length < SEP_PART_SIZE){
            throw new IllegalArgumentException("uI array size is too small :" + uI.length);
        }
        List<Graph> graphs = new ArrayList<Graph>();

        Graph zeroTimeGraph = new Graph();
        for (double i = 0; i < SEP_PART_SIZE; i++) {
            for (double j = 0; j < SEP_PART_SIZE; j++){
                Point p = new Point(i/SEP_PART_SIZE, j/SEP_PART_SIZE, uI[(int)i]);
                zeroTimeGraph.add(p);
            }
        }
        graphs.add(zeroTimeGraph);

        //j is time counter
        Graph prevGraph = zeroTimeGraph;
        Graph currentGraph = null;
        for (int j = 1; j <= resultTime / DELTA_T; j ++){
            currentGraph = new Graph(function);
            for (double i = 1; i < SEP_PART_SIZE; i ++){
                for (double k = 1; k < SEP_PART_SIZE; k ++){
                    //prevGraph = graphs.get(j - 1);
                    double currentX = i/SEP_PART_SIZE;
                    double currentY = k/SEP_PART_SIZE;

                    double prevUX = 0, prevUY = 0,
                            nextUX = 0, nextUY = 0;
                    try{
                        prevUX = i == 1
                                ? g1Method(j)
                                : prevGraph.getByXY(currentX - deltaH, currentY);
                        nextUX = i == SEP_PART_SIZE - 1
                                ? g2Method(j)
                                : prevGraph.getByXY(currentX + deltaH, currentY);

                        prevUY = k == 1
                                ? g1Method(k)
                                : prevGraph.getByXY(currentX, currentY - deltaH);
                        nextUY = k == SEP_PART_SIZE - 1
                                ? g2Method(k)
                                : prevGraph.getByXY(currentX, currentY + deltaH);
                    } catch (Exception e){
                        System.out.println(" i = " + i +"j = " + j +"k = " + k );
                        e.printStackTrace();
                    }

                    double uCurrent = prevGraph.getByXY(currentX, currentY);
                    double actualU = resolveByFormula(uCurrent,
                            prevUX, nextUX,
                            prevUY, nextUY,
                            dCoef,vCoef,
                            func.evaluate(currentX, currentY));
                    //todo: this point should adds in the end of "x y cycles"
                    currentGraph.add(new Point(currentX, currentY, actualU));
                }
            }
            prevGraph = currentGraph;
        }
        return currentGraph;
    }

    public double g1Method(double t){
        return 0;
    }

    public double g2Method(double t){
        return 0;
    }

    private Double resolveByFormula(double uCurrent, double uPrevX,
                                    double uNextX, double uPrevY,
                                    double uNextY, double dCoef,
                                    double vCoef, double functionInHere){
        return ((dCoef * (uNextX - 4 * uCurrent + uPrevX + uNextY + uPrevX) * (1/SEP_PART_SIZE)
                + vCoef * (uNextX + uNextY - 2 * uCurrent)) * (1/SEP_PART_SIZE)  + functionInHere)
                * DELTA_T + uCurrent;


    }


}
