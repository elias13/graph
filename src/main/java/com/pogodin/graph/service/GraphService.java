package com.pogodin.graph.service;

import com.pogodin.graph.model.Graph;

/**
 * Created by illia
 * 15.06.13 at 0:21
 */
public interface GraphService {

    double SEP_PART_SIZE = 20.0;
    double DELTA_T = 0.01;


    Graph resolveCoordMethod(final double [] uI,
                                    final double dCoef,
                                    final double vCoef,
                                    final double resultTime,
                                    final String function);
}
