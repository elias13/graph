package com.pogodin.graph.controller;

import com.pogodin.graph.model.Graph;
import com.pogodin.graph.model.Point;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/graph")
public class MainPageController {
	@RequestMapping(value = "points", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		return "main";
	}

    @RequestMapping(method = RequestMethod.GET)
    public String printTestModel(ModelMap model) {
        model.put("graphParams", testMe().jsonArray());
        return "test";
    }

    private Graph testMe(){
        Graph result = new Graph();
        for(double x = 0; x < 1; x += Math.PI/100){
            for(double y = 0; y < 1; y += Math.PI/100){
                result.add(new Point(x, y, Math.sin(x)+Math.cos(y)));
            }
        }
        return result;
    }


}