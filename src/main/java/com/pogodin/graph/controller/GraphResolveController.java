package com.pogodin.graph.controller;

import com.pogodin.graph.service.GraphService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/graph")
public class GraphResolveController {

    @Autowired
    private GraphService service;
            
    /*@RequestMapping(method = RequestMethod.POST)
    public @ResponseBody String resolveGraph(@RequestParam String somedata){
        System.out.println(somedata);
        return service.generateGraph(somedata).jsonArray();
    }*/

    @RequestMapping(value = "/{function}/{time}/{dCoef}/{vCoef}", method = RequestMethod.POST)
    public @ResponseBody String resolveGraphWithMethod(@PathVariable String function,
                                                       @PathVariable double time,
                                                       @PathVariable int dCoef,
                                                       @PathVariable int vCoef){
        double ui[] = new double[(int)GraphService.SEP_PART_SIZE];
        ui[0] = 0;
        for (int i = 1; i < ui.length; i ++){
            ui[i] = 0;//0.01 + ui[i - 1];
        }
        return service.resolveCoordMethod(ui, dCoef, vCoef, time, function).jsonArray();
    }

    public void setService(GraphService service) {
        this.service = service;
    }


}
