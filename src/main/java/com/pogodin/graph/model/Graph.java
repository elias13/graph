package com.pogodin.graph.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {

    private static final int COEFITIENT = 10000;
    private List<Point> points;
    private String color;
    private Map<Double, Double> pointsInMap;
    private String additionalFunction;

    public Graph(){
        points = new ArrayList();
        pointsInMap = new HashMap<Double, Double>();
    }

    public Graph(String additionalFunction) {
        this();
        this.additionalFunction = additionalFunction;
    }

    public Graph(List<Point> points , String additionalFunction) {
        this.points = points;
        initMapFromPoints();
        this.additionalFunction = additionalFunction;
    }

    private void initMapFromPoints(){
        pointsInMap = new HashMap<Double, Double>();
        for (Point point : points) {
            putIntoPointMap(point);
        }
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getAdditionalFunction() {
        return additionalFunction;
    }

    public void setAdditionalFunction(String additionalFunction) {
        this.additionalFunction = additionalFunction;
    }

    public void add(Point point){
        if(point != null){
            points.add(point);
            putIntoPointMap(point);
        }
    }

    private void putIntoPointMap(Point point){
        pointsInMap.put(point.getX() + point.getY() * COEFITIENT, point.getZ());
    }

    public void addNext(double x, double y, double z){
        add(new Point(x, y, z));
    }

    public Double getByXY(double x, double y){
        x = Math.round(x * 100) / 100.0;
        y = Math.round(y * 100) / 100.0;
        return pointsInMap.get(x + y * COEFITIENT);
    }

    @Override
    public String toString() {
        return "Graph{" +
                "points=" + points +
                ", color='" + color + '\'' +
                '}';
    }

    public String jsonArray(){
        if(points == null || points.isEmpty()){
            return "[]";
        }
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        String prefix = "";
        for (Point point : points) {
            builder.append(prefix);
            prefix = ",";
            builder.append(point.json());
        }
        builder.append("]");
        return builder.toString();
    }
}
