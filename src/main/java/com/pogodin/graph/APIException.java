package com.pogodin.graph;

/**
 * Created by illia
 * 09.06.13 at 15:33
 */
public class APIException extends RuntimeException {
    public APIException(String message) {
        super(message);
    }
}

