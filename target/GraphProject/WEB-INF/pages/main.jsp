<%@ include file="taglib.jsp" %>
<%@ page language="java" contentType="text/html; charset=utf-8" %>
<meta charset='utf-8'>
<!DOCTYPE html>

<html lang="en" manifest="/cache.manifest">
<head>

</head>
<body>

<div class="container">

    <br>
    <br>

    <div class="row main_content">

        <div class="span3">
            <div class="well">
                <form action="/graph" method="post">
                    <nav>
                        <ul id="categoryList" class="nav nav-list">
                            <li class="nav-header" disable="disable">
                                сюда вводить данные
                            </li>
                            <li>
                                here is some data
                                <input type="text"/>
                            </li>
                            <li>
                                <input type="submit" class="btn btn-primary btn-block" value="Рисовать">
                            </li>
                        </ul>
                    </nav>
                </form>
            </div>
        </div>

        <div class="span9">
            <div id="fileInfoContent" class="well text-center">
                <jsp:include page="graph.jsp" />
            </div>
        </div>
    </div>
</div>

</body>

<script type="text/javascript">

    function hello(){

    }


</script>
</html>